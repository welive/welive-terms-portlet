package com.welive.terms.portlet;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class TermsPortlet
 */
public class TermsPortlet extends MVCPortlet {
	
	public static final String KEY_BACK = "back";

	@Override
	public void doView(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {
		HttpServletRequest httpRequest = PortalUtil.getOriginalServletRequest(PortalUtil.getHttpServletRequest(renderRequest));
		String pageBack = httpRequest.getParameter(KEY_BACK);
		
		if (pageBack != null) {
			PortletSession session = renderRequest.getPortletSession();
			session.setAttribute(KEY_BACK, pageBack);
		}
		
		super.doView(renderRequest, renderResponse);
		
//		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
	}
	
	public void back(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException, PortletException {
		PortletSession session = actionRequest.getPortletSession();
		String pageBack = (String)session.getAttribute(KEY_BACK);
		if(pageBack == null) {
			ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			pageBack = themeDisplay.getURLHome();
		}
		
		SessionMessages.add(actionRequest, getPortletConfig().getPortletName() + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_SUCCESS_MESSAGE);
		actionResponse.sendRedirect(pageBack);
	}
}
