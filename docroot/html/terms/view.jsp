<%@page import="java.util.ResourceBundle" %>
<%@page import="com.liferay.portal.util.PortalUtil"%>

<%@taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<portlet:actionURL var="backURL" windowState="normal" name="back">
</portlet:actionURL>

<%
String casLoginUrl = "/c/portal/login";
boolean login = false;
try {
	if(user == null || !themeDisplay.isSignedIn()) {
		HttpServletRequest httpRequest = PortalUtil.getOriginalServletRequest(request);
		if("true".equals(httpRequest.getParameter("login"))) {
			login = true;
		}
	}
} catch(Exception e) {}
%>

<%
ResourceBundle rb = portletConfig.getResourceBundle(locale);
String pageTerms = rb.getString("terms.page");
%>

<c:choose>
	
	<c:when test="<%=login%>">
		<script>
			location.replace("<%=casLoginUrl%>");
		</script>
	</c:when>
	<c:otherwise>

		<jsp:include page="<%= pageTerms %>" flush="true" />
	
	</c:otherwise>
</c:choose>
			<div id="materialize-body" class="materialize">
				<div class="section">
					
					<a class="red lighten-1 white-text btn btn-flat" href="<%=backURL%>"><liferay-ui:message key="terms.back" /></a>
										
				</div>
			</div>
